import React, {useState, useEffect} from 'react';
import axios from 'axios';
import MaximizeContent from '../components/MaximizeContent';

export const NoteProvider = () => {
    const [state, setState] = useState([]);
    
    const getData = async () => {
        try {
            await axios.get('https://cors-anywhere.herokuapp.com/https://anapioficeandfire.com/api/characters/583')
            .then(data => setState(data.data))            
        } catch (error) {
            console.log(`Unable to get: ${error}`)
        }
    }
    useEffect(() => {
        getData()
    }, [])

    return (
        <>
        <h3 className="text-center">GAME OF THRONES - JON SNOW NOTE</h3>
        <h4 className="text-center">A Clickable Note Using React Hooks</h4>
        <MaximizeContent >
            <div className="paper row flex-middle border border-thick border-5 shadow" style={{width: "20rem"}}>
                <div className="paper-body">
                    <h4 className="card-title text-center" style={{ marginBottom: 0, marginTop: 0}}>{state.name}</h4>
                    <h5 className="card-subtitle text-center" style={{ marginTop: 0, color: 'darkgreen'}} >Title: {state.titles}</h5>
                    <p className="card-text">Culture: {state.culture}</p>
                    <p className="card-text">Born: {state.born}</p>
                    <p className="card-text">Died: {state.died}</p>
                    <p className="card-text">Gender: {state.gender}</p>
                    <p className="card-text">Played by: {state.playedBy}</p>
                </div>
            </div>
        </MaximizeContent>
        <footer className="text-center"><h6>&copy; Jocelyn Bergdorff, 2020</h6></footer>
        </>
    )
}